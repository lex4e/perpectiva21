public class Program {
	public static void main (String[] args) {
//		Dog dog = new Dog();
//		dog.run();
//		System.out.println("Nick = " + dog.getNickname());

		RunnableInterface[] animals = {new Dog(), new Cat(), new Bug()};

		Program program = new Program();

		for (RunnableInterface animal : animals) {
			program.runDistance(3, animal);
		}
	}

	public void runDistance(int distance, RunnableInterface animals) {
		for (int i=0; i < distance; i++) {
			animals.run();
		}
	}
}
